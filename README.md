# Plate Watcher

Plate Watcher is an Android application to lookup cars by their license plates in an offline database stored on your smartphone. It allows users to create and edit car license plate databases on their devices locally. Users can also import existing databases from simple 3-column table in .csv format:
```
"<NUMBER>","<COLOR>","<Description>"
```

At this moment, only manual entry is supported.
We are planning to add license plate recognition using the smartphone camera in the later versions, once a well-working License Plate Recognition implementation is available for Android.

### Disclaimer

This is my first Native Android application, I am new to Kotlin language and my Android skills are a bit rusty. The source code definitely needs improvement and refactoring, especially to utilize the recommended ViewModel approach.

## Building

*Assuming you have pulled the sources and your shell's current working dir is the root of the source code repository*

1. Ensure that OpenJDK 17, wget and unzip are installed
```shell
# On Debian-based distributions
sudo apt install openjdk-17-jdk wget unzip
```
2. Configure Environment Variables
```shell
# An example path to openjdk-17 java HOME on Ubuntu, may differ in other distributions
export JAVA_HOME=/usr/lib/jvm/java-17-openjdk-amd64

export ANDROID_HOME="${PWD}/android-sdk-root"
export ANDROID_COMPILE_SDK=33
export ANDROID_BUILD_TOOLS="33.0.2"
export ANDROID_SDK_TOOLS=9477386

install -d $ANDROID_HOME
```
3. Download and install Android SDK tools
```shell
wget --output-document=$ANDROID_HOME/cmdline-tools.zip https://dl.google.com/android/repository/commandlinetools-linux-${ANDROID_SDK_TOOLS}_latest.zip

unzip -q -d "$ANDROID_HOME/cmdline-tools" "$ANDROID_HOME/cmdline-tools.zip"

mv -T "$ANDROID_HOME/cmdline-tools/cmdline-tools" "$ANDROID_HOME/cmdline-tools/tools"
```

4. Add Android cmdline-tools to your build environment
```shell
export PATH=$PATH:$ANDROID_HOME/cmdline-tools/latest/bin:$ANDROID_HOME/cmdline-tools/tools/bin
```

5. Get the Android SDK
```shell
# use yes to accept all licenses
yes | sdkmanager --licenses > /dev/null || true

sdkmanager "platforms;android-${ANDROID_COMPILE_SDK}"
sdkmanager "platform-tools"
sdkmanager "build-tools;${ANDROID_BUILD_TOOLS}"
```

6. Run the build using gradle
```shell
chmod +x ./gradlew
./gradlew assembleDebug
```

After the build is done, you may find the app-debug.apk package in the app/build/outputs/apk/debug/ subfolder.

## Installation

Upload APK on your phone and pick it using your File Manager.

As an alternative way, enable Developer mode and install it using adb tool:

```shell
adb install <path to app-debug.apk>
```

## Usage

1. Click the Preferences Icon on the top action bar
2. Either import car data from a .csv file or create and edit groups of car data manually

In the main view, enter a license plate number to lookup for.

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

## License

[GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)