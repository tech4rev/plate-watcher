/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr

fun cleanupLP(plate: String): String = plate.uppercase()
    .filter { it.isDigit() || it.isUpperCase() || it == '-' }

fun ambiguities(c: Char): Array<Char> {
    return when (c) {
        '0', 'O', 'Ö' -> arrayOf('0', 'O', 'Ö')
        'Z', '2', '7' -> arrayOf('Z', '2', '7')
        '1', 'I' -> arrayOf('1', 'I')
        'A','Ä' -> arrayOf('A', 'Ä')
        'U', 'Ü' -> arrayOf('U', 'Ü')
        '8', 'B', 'H' -> arrayOf('8', 'B', 'H')
        '6', 'G' -> arrayOf('6', 'G')
        else -> arrayOf(c)
    }
}

private fun isAmbiguous(c: Char): Boolean = when (c) {
    'A', 'Ä', 'U', 'Ü', '0', 'O', 'Ö', 'Z', '2', '7', '1', 'I', '8', 'B', 'H', '6', 'G' -> true
    else -> false
}

fun combineNumbers(s: String): List<String> {
    fun combineNumbers(pos: Int): List<String> {
        if (pos == s.length) return listOf("")
        val result = ArrayList<String>()
        val combos = combineNumbers(pos + 1)
        for (c in ambiguities(s[pos])) {
            for (combo in combos) {
                result.add(c + combo)
            }
        }
        return result
    }
    if (s.none { isAmbiguous(it) }) return listOf(s)
    return combineNumbers(0)
}

fun getCleanCombinations(plate: String): List<String> = combineNumbers(cleanupLP(plate))
