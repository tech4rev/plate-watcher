/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "GROUPS")
data class GroupRecord(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo("ID") val id: Long,
    @ColumnInfo("NAME") val name: String,
    @ColumnInfo("USED") val used: Boolean
)


@Entity
data class GroupRecordFull(
    @ColumnInfo(name = "ID") val id: Long,
    @ColumnInfo(name = "NAME") val name: String,
    @ColumnInfo(name = "USED") val used: Boolean,
    @ColumnInfo(name = "COUNT") val count: Int
) {
    fun asGroupRecord(): GroupRecord {
        return GroupRecord(id, name, used)
    }
}

