/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update


@Dao
interface GroupsDao {
    @Insert
    suspend fun insert(groupRecord: GroupRecord): Long

    @Insert
    fun insertRaw(groupRecord: GroupRecord): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(plateRecord: GroupRecord)

    @Delete
    suspend fun delete(group: GroupRecord)

    @Delete(GroupRecord::class)
    suspend fun deleteById(id: LongID)

    @Delete(GroupRecord::class)
    fun deleteByIdRaw(id: LongID)


    @Query("SELECT * FROM GROUPS WHERE ID = :id")
    suspend fun getGroup(id: Long): GroupRecord?

    @Query("SELECT GROUPS.*, COUNT(PLATES.ID) as COUNT FROM GROUPS LEFT JOIN PLATES ON PLATES.GROUP_ID=GROUPS.ID GROUP BY GROUPS.ID")
    suspend fun getGroupsFull(): List<GroupRecordFull>
}
