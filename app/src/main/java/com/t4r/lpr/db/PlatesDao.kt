/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.db

import androidx.room.ColumnInfo
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Entity
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update


data class LongID(
    @ColumnInfo("ID") val id: Long,
)

data class PlateWithGroupName(
    @ColumnInfo("ID") val id: Long,
    @ColumnInfo("NUMBER") val number: String,
    @ColumnInfo("COLOR") val color: String,
    @ColumnInfo("EXTRA") val extra: String,
    @ColumnInfo("GROUP_ID") val groupId: Long,
    @ColumnInfo("GROUP_NAME") val groupName: String
)

@Dao
interface PlatesDao {
    @Insert
    suspend fun insert(plateRecord: PlateRecord): Long

    @Insert
    fun insertRaw(plateRecord: PlateRecord): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(plateRecord: PlateRecord)

    @Delete
    suspend fun delete(plateRecord: PlateRecord)

    @Delete(PlateRecord::class)
    suspend fun deleteById(id: LongID)

    @Query("SELECT * FROM PLATES WHERE ID = :id")
    suspend fun getPlate(id: Long): PlateRecord

    @Query("SELECT * FROM PLATES WHERE GROUP_ID = :groupID")
    suspend fun getGroupPlates(groupID: Long): List<PlateRecord>

    @Query("SELECT * FROM PLATES WHERE NUMBER = :number")
    fun lookupPlatesByNumberRaw(number: String): List<PlateRecord>

    @Query("SELECT PLATES.*, GROUPS.NAME as GROUP_NAME FROM PLATES,GROUPS WHERE PLATES.GROUP_ID = GROUPS.ID AND GROUPS.USED = 1 AND NUMBER = :number")
    fun lookupPlatesInUsedGroupsByNumberRaw(number: String): List<PlateWithGroupName>
}
