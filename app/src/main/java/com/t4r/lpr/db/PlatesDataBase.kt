/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [GroupRecord::class, PlateRecord::class], version = 2)
abstract class PlatesDataBase : RoomDatabase() {
    abstract fun platesDao(): PlatesDao

    abstract fun groupsDao(): GroupsDao
}

fun buildDatabase(applicationContext: Context) = Room.databaseBuilder(
    applicationContext, PlatesDataBase::class.java, "plates-db"
).build()
