package com.t4r.lpr.engine

import org.opencv.core.Scalar

const val TAG = "LPR"

val FRAME_COLOR = Scalar(100.0, 200.0, 0.0)
val FRAME_COLOR2 = Scalar(0.0, 200.0, 255.0)
val FRAME_COLOR3 = Scalar(200.0, 200.0, 0.0)

const val ALG_WIDTH_PRESET = 1024
const val ALG_HEIGHT_PRESET = 576
const val MIN_ALG_AREA = ALG_HEIGHT_PRESET * ALG_WIDTH_PRESET / 500.0
const val SHIELD_WIDTH = 520
const val SHIELD_HEIGHT = 110
const val CANNY_THRESHOLD_MIN = 30.0
const val CANNY_THRESHOLD_MAX = 200.0
const val CANNY_APERTURE_SIZE = 3

const val MAX_FRAME_WIDTH = 1280
const val MAX_FRAME_HEIGHT = 720