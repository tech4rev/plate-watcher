package com.t4r.lpr.engine

import android.util.Log
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.MatOfPoint
import org.opencv.core.MatOfPoint2f
import org.opencv.core.Point
import org.opencv.core.Rect
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt

data class RectanglePoints(
    val topLeft: Point, val topRight: Point, val bottomLeft: Point, val bottomRight: Point
)

data class Contour(
    val contour: MatOfPoint,
    val area: Double = (Imgproc.contourArea(contour)).absoluteValue,
    val averageX: Double = averageX(contour),
    val averageY: Double = averageY(contour)
)

fun averageX(contour: MatOfPoint): Double {
    val numPoints = contour.cols()
    var avg = 0.0
    for (idx in 0..<numPoints) {
        avg += contour[0, idx][0]
    }
    return avg
}

fun averageY(contour: MatOfPoint): Double {
    val numPoints = contour.cols()
    var avg = 0.0
    for (idx in 0..<numPoints) {
        avg += contour[0, idx][1]
    }
    return avg
}

class FrameProcessor {
    private var wRatio = 1.0
    private var hRatio = 1.0

    lateinit var shieldImage: Mat
    var shieldFound = false

    private lateinit var edged: Mat
    private lateinit var resized: Mat
    private lateinit var resizedFiltered: Mat

    private lateinit var rectangle: MatOfPoint2f
    private lateinit var foundContour: MatOfPoint

    private var contours = arrayOf<Contour?>()
    private lateinit var frameRGBA: Mat
    private lateinit var frameGray: Mat


    fun initLargeArrays() {
        shieldImage = Mat(SHIELD_HEIGHT, SHIELD_WIDTH, CvType.CV_32S)
        edged = Mat(algHeight, algWidth, CvType.CV_8U)
        resized = Mat(algWidth, algHeight, CvType.CV_8U)
        resizedFiltered = Mat(algWidth, algHeight, CvType.CV_8U)
        rectangle = MatOfPoint2f()
    }

    fun destroyLargeArrays() {
        shieldImage.release()
        edged.release()
        resized.release()
        resizedFiltered.release()
        rectangle.release()
    }

    private var isPortrait = false
    private var algWidth = ALG_WIDTH_PRESET
    private var algHeight = ALG_HEIGHT_PRESET
    fun process(inputFrame: CvCameraViewFrame): Mat {
        extractFrame(inputFrame)
        preprocessInputFrame()
        extractSortedContours()
        shieldFound = false
        if (findPotentialLicensePlate()) {
            shieldFound = true
            drawFoundPlate()
            drawShield(frameRGBA)
        }
        val frame = frameRGBA
        frameRGBA = Mat()
        frameGray = Mat()
        return frame
    }

    private fun drawShield(frame: Mat) {
        if (frame.width() < SHIELD_WIDTH || frame.height() < SHIELD_HEIGHT) return

        val roi = Rect(
            0,
            frame.height() - SHIELD_HEIGHT,
            minOf(SHIELD_WIDTH, frame.width()),
            SHIELD_HEIGHT
        )
        val targetMat = Mat(frame, roi)
        shieldImage.copyTo(targetMat)
        targetMat.release()
    }

    private fun drawApproxRectOnFrame(
        topLeft: Point, topRight: Point, bottomRight: Point, bottomLeft: Point
    ) {
        val drawableContour = MatOfPoint(topLeft, topRight, bottomRight, bottomLeft)
        Imgproc.drawContours(frameRGBA, listOf(drawableContour), -1, FRAME_COLOR, 3)
        drawableContour.release()
    }

    private fun drawExactContourOnFrame() {
        val cArray = foundContour.toArray()
        cArray.forEach { p ->
            p.x *= wRatio
            p.y *= hRatio
        }
        val contour = MatOfPoint(*cArray)
        Imgproc.drawContours(frameRGBA, listOf(contour), -1, FRAME_COLOR2, 3)
        contour.release()
    }

    private fun drawFoundPlate() {
        val (topLeft, topRight, bottomLeft, bottomRight) = getScaledApproxRectPoints()
        extractShieldImage(topLeft, topRight, bottomLeft, bottomRight)
        drawApproxRectOnFrame(topLeft, topRight, bottomRight, bottomLeft)
        drawExactContourOnFrame()
    }

    private fun extractFrame(inputFrame: CvCameraViewFrame) {
        frameGray = inputFrame.gray()
        frameRGBA = inputFrame.rgba()
        val oldIsPortrait = isPortrait
        isPortrait = frameGray.width() < frameGray.height()
        if (isPortrait != oldIsPortrait) {
            destroyLargeArrays()
            initLargeArrays()
        }
        algWidth = if (isPortrait) ALG_HEIGHT_PRESET else ALG_WIDTH_PRESET
        algHeight = if (isPortrait) ALG_WIDTH_PRESET else ALG_HEIGHT_PRESET
        wRatio = frameRGBA.width().toDouble() / algWidth.toDouble()
        hRatio = frameRGBA.height().toDouble() / algHeight.toDouble()
    }

    private fun preprocessInputFrame() {
        Imgproc.resize(frameGray, resized, Size(algWidth.toDouble(), algHeight.toDouble()))
        Imgproc.Canny(resized, edged, CANNY_THRESHOLD_MIN, CANNY_THRESHOLD_MAX, CANNY_APERTURE_SIZE)
    }

    private object AreaComparator : Comparator<Contour?> {
        override fun compare(o1: Contour?, o2: Contour?): Int {
            o1?.let { o2?.let { return o1.area.compareTo(o2.area) } }
            return -1
        }
    }

    private class CenterComparator(val algWidth: Double, val algHeight: Double) :
        Comparator<Contour?> {
        override fun compare(c1: Contour?, c2: Contour?): Int {
            c1?.let {
                c2?.let {
                    return Math.sqrt(
                        (c1.averageX - this.algWidth / 2).pow(2) + (c1.averageY - algHeight / 2).pow(
                            2
                        )
                    ).compareTo(
                        Math.sqrt(
                            (c2.averageX - algWidth / 2).pow(2) + (c2.averageY - algHeight / 2).pow(
                                2
                            )
                        )
                    )
                }
            }
            return -1
        }
    }

    private fun extractSortedContours() {
        val hierarchy = Mat()
        val foundContours = arrayListOf<MatOfPoint>()
        Imgproc.findContours(
            edged, foundContours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE
        )
        contours = arrayOfNulls(foundContours.size)
        var idx = 0
        for (foundContour in foundContours) {
            val cntObj = Contour(foundContour)
            if (cntObj.area >= MIN_ALG_AREA) {
                contours[idx++] = cntObj
            }
        }
        contours = contours.sliceArray(IntRange(0, idx - 1))
        contours.sortWith(AreaComparator)
        contours.sortWith(CenterComparator(algWidth.toDouble(), algHeight.toDouble()))
    }

    private fun findPotentialLicensePlate(): Boolean {
        val approx = MatOfPoint2f()
        for (cnt in contours) {
            if (cnt == null) continue

            val curve = MatOfPoint2f()
            curve.alloc(cnt.contour.elemSize().toInt())
            cnt.contour.convertTo(curve, CvType.CV_32F)
            val perimeter = Imgproc.arcLength(curve, true)

            Imgproc.approxPolyDP(curve, approx, 0.015 * perimeter, true) //0.018
            if (approx.total() == 4L) {
                val p1 = getPointFromMat(approx, 0)
                val p2 = getPointFromMat(approx, 1)
                val p3 = getPointFromMat(approx, 2)
                val p4 = getPointFromMat(approx, 3)
                val c = MatOfPoint(p1, p2, p3, p4)
                if (!Imgproc.isContourConvex(c)) continue

                val d1 = sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))
                val d2 = sqrt((p2.x - p3.x) * (p2.x - p3.x) + (p2.y - p3.y) * (p2.y - p3.y))
                val d3 = sqrt((p3.x - p4.x) * (p3.x - p4.x) + (p3.y - p4.y) * (p3.y - p4.y))
                val d4 = sqrt((p4.x - p1.x) * (p4.x - p1.x) + (p4.y - p1.y) * (p4.y - p1.y))
                val ratio1 = max(d1, d2) / min(d1, d2)
                val ratio2 = max(d3, d4) / min(d3, d4)
                if ((ratio1 < 12) && (ratio1 > 3) && (ratio2 < 12) && (ratio2 > 3)) {
                    rectangle = approx
                    foundContour = cnt.contour
                    return true
                }
            }
        }
        return false
    }

    private fun getScaledApproxRectPoints(): RectanglePoints {
        val points = rectangle.toArray()
        points.forEach { p ->
            p.x *= wRatio
            p.y *= hRatio
        }
        points.sortWith { o1, o2 ->
            o1.x.compareTo(o2.x)
        }
        val (topLeft, bottomLeft) = when (points[0].y < points[1].y) {
            true -> points[0] to points[1]
            false -> points[1] to points[0]
        }
        val (topRight, bottomRight) = when (points[2].y < points[3].y) {
            true -> points[2] to points[3]
            false -> points[3] to points[2]
        }
        return RectanglePoints(topLeft, topRight, bottomLeft, bottomRight)
    }

    private fun extractShieldImage(
        topLeft: Point?, topRight: Point?, bottomLeft: Point?, bottomRight: Point?
    ) {
        val srcPoints = MatOfPoint2f(topLeft, topRight, bottomLeft, bottomRight)
        val dstPoints = MatOfPoint2f(
            Point(0.0, 0.0),
            Point(SHIELD_WIDTH.toDouble(), 0.0),
            Point(0.0, SHIELD_HEIGHT.toDouble()),
            Point(SHIELD_WIDTH.toDouble(), SHIELD_HEIGHT.toDouble())
        )

        val perspectiveMatrix = Imgproc.getPerspectiveTransform(srcPoints, dstPoints)
        srcPoints.release()
        dstPoints.release()

        Imgproc.warpPerspective(
            frameRGBA,
            shieldImage,
            perspectiveMatrix,
            Size(SHIELD_WIDTH.toDouble(), SHIELD_HEIGHT.toDouble())
        )
        perspectiveMatrix.release()
    }

    private fun getPointFromMat(m: MatOfPoint2f, idx: Int): Point {
        val data = m.get(idx, 0)
        return Point(data[0], data[1])
    }
}