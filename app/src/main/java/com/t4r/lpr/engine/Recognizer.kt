package com.t4r.lpr.engine

import android.graphics.Bitmap
import android.util.Log
import com.google.mlkit.vision.common.InputImage

import com.google.mlkit.vision.text.Text
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.TextRecognizer
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import org.opencv.core.Scalar

class Recognizer {
    private lateinit var recognizer: TextRecognizer

    fun init() {
        recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
    }

    private var onPlateRecognized: (String) -> Unit = {}
    fun setOnPlateRecognizedCallback(callback: (String) -> Unit) {
        onPlateRecognized = callback
    }

    fun process(bitmap: Bitmap) {
        val img = InputImage.fromBitmap(bitmap, 0)
        recognizer.process(img).addOnSuccessListener {
            val textBlocks = it.textBlocks
            val recognizedString = filteredOCRResult(textBlocks)
            Log.i(TAG, "recognized: '$recognizedString'")
            if (recognizedString.isNotEmpty()) {
                onPlateRecognized(recognizedString)
            }
        }
    }

    private fun filteredOCRResult(
        textBlocks: List<Text.TextBlock>
    ): String {
        var recognizedString1 = ""
        for (block in textBlocks) {
            block.boundingBox?.let { box ->
                Log.i(TAG, "block.height=${box.height()} text='${block.text}'")
                if ((box.height() > (2 * SHIELD_HEIGHT / 3)) and (box.width() > (2 * SHIELD_WIDTH / 3))) {
                    for (line in block.lines) {
                        line.boundingBox?.let { lineBBox ->
                            if (lineBBox.height() > (2 * SHIELD_HEIGHT / 3)) {
                                for (element in line.elements) {
                                    element.boundingBox?.let { elBBox ->
                                        if (elBBox.height() > (2 * SHIELD_HEIGHT / 3)) {
                                            Log.i(
                                                TAG,
                                                "element.height=${elBBox.height()} text='${element.text}'"
                                            )
                                            for (symbol in element.symbols) {
                                                symbol.boundingBox?.let { symBBox ->
                                                    /*val r = Rect(
                                                        symBBox.left,
                                                        symBBox.top,
                                                        symBBox.width(),
                                                        symBBox.height()
                                                    )
                                                    Imgproc.rectangle(
                                                        processor.shieldImage, r, FRAME_COLOR3, 4
                                                    )*/
                                                    //Log.i(
                                                    //    TAG,
                                                    //    "symbol.bbox = $symBBox r=$r symbol.height=${symBBox.height()} text='${symbol.text}' confidence = ${symbol.confidence}'"
                                                    //)
                                                    if ((symbol.confidence > CONFIDENCE_THRESHOLD) && symBBox.height() > (2 * SHIELD_HEIGHT / 3)) {
                                                        recognizedString1 += symbol.text
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return recognizedString1
    }

    companion object {
        const val CONFIDENCE_THRESHOLD = 0.25
    }
}
