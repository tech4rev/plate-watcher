/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.view

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.hardware.camera2.CameraAccessException
import android.hardware.camera2.CameraManager
import android.hardware.camera2.CameraManager.TorchCallback
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.SurfaceView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doAfterTextChanged
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.t4r.lpr.R
import com.t4r.lpr.cleanupLP
import com.t4r.lpr.databinding.ActivityMainBinding
import com.t4r.lpr.db.PlateRecord
import com.t4r.lpr.db.buildDatabase
import com.t4r.lpr.engine.FrameProcessor
import com.t4r.lpr.engine.MAX_FRAME_HEIGHT
import com.t4r.lpr.engine.MAX_FRAME_WIDTH
import com.t4r.lpr.engine.Recognizer
import com.t4r.lpr.viewmodel.LookupViewModel
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.Mat
import org.opencv.core.Point
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc

class MainActivity : AppCompatActivity(), CvCameraViewListener2 {
    companion object {
        const val MANUAL_VISIBLE_KEY = "MANUAL_VISIBLE"
        const val HIDE_CATEGORY_KEY = "HIDE_CATEGORY"
        const val FLASHLIGHT_VISIBLE_KEY = "FLASHLIGHT_CONTROL_VISIBLE"
        const val CAMERA_CONTROL_VISIBLE_KEY = "CAMERA_CONTROL_VISIBLE"
        private const val TAG = "PlateScan"
        private val REQUEST_PERMISSIONS =
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) arrayOf(
                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA
            ) else arrayOf(Manifest.permission.CAMERA)
    }

    private lateinit var mOpenCvCameraView: CameraBridgeViewBase
    private val processor = FrameProcessor()
    private val recognizer = Recognizer()

    private lateinit var viewBinding: ActivityMainBinding

    private val activityResultLauncher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            var permissionGranted = true
            permissions.entries.forEach {
                if (!it.value && (it.key in REQUEST_PERMISSIONS)) permissionGranted = false
                if (it.value && it.key == Manifest.permission.CAMERA) {
                    onCameraPermissionsGranted()
                }
            }
            if (!permissionGranted) {
                Toast.makeText(
                    baseContext, "Permission request denied", Toast.LENGTH_SHORT
                ).show()
            }
        }

    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var cameraManager: CameraManager
    private lateinit var cameraId: String
    private lateinit var torchCallback: TorchCallback

    private var listener: SharedPreferences.OnSharedPreferenceChangeListener =
        SharedPreferences.OnSharedPreferenceChangeListener { _, key ->
            when (key) {
                MANUAL_VISIBLE_KEY -> updateManualInputVisibility()
                HIDE_CATEGORY_KEY -> viewModel.hideGroupInResult.postValue(
                    sharedPreferences.getBoolean(
                        key, false
                    )
                )

                FLASHLIGHT_VISIBLE_KEY -> updateFlashlightControlVisibility()
                CAMERA_CONTROL_VISIBLE_KEY -> updateCameraControlVisibility()
            }
        }

    private val viewModel: LookupViewModel by viewModels()

    private fun onInputChanged() {
        Log.i(TAG, "onInputChanged()")
        val input = viewBinding.manualPlateInput.text.toString()
        val cleanInput = cleanupLP(input)
        viewModel.currentLookupStringClean.postValue(cleanInput)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewModel.setDBModel(buildDatabase(applicationContext))

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        Log.i(TAG, "sharedPreferences=$sharedPreferences")

        viewBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        setSupportActionBar(viewBinding.mainActionBar)
        viewBinding.clearManualPlateBtn.setOnClickListener {
            viewBinding.manualPlateInput.text.clear()
        }

        viewBinding.manualPlateInput.doAfterTextChanged {
            onInputChanged()
        }

        val decoration = DividerItemDecoration(
            this, LinearLayoutManager.VERTICAL
        )
        viewBinding.exactResults.layoutManager = LinearLayoutManager(this)
        viewBinding.exactResults.addItemDecoration(decoration)
        viewBinding.fuzzyResults.layoutManager = LinearLayoutManager(this)
        viewBinding.fuzzyResults.addItemDecoration(decoration)

        viewBinding.exactMatchGroup.visibility = View.GONE
        viewBinding.fuzzyMatchGroup.visibility = View.GONE

        viewModel.shownExactLookupResult.observe(this) { exactMatches ->
            Log.i(TAG, "Exact results: $exactMatches")
            viewBinding.exactMatchGroup.visibility =
                if (exactMatches.isEmpty()) View.GONE else View.VISIBLE
            val resultAdapter = ResultAdapter(exactMatches)
            viewBinding.exactResults.adapter = resultAdapter
            if (exactMatches.isNotEmpty() && cleanupLP(viewBinding.manualPlateInput.text.toString()) != viewModel.currentLookupStringClean.value) {
                viewBinding.manualPlateInput.setText(viewModel.currentLookupStringClean.value)
            }
        }
        viewModel.showFuzzyLookupResult.observe(this) { fuzzyMatches ->
            Log.i(TAG, "Fuzzy results: $fuzzyMatches")
            viewBinding.fuzzyMatchGroup.visibility =
                if (fuzzyMatches.isEmpty()) View.GONE else View.VISIBLE
            val resultAdapter = ResultAdapter(fuzzyMatches)
            viewBinding.fuzzyResults.adapter = resultAdapter
        }

        viewModel.cameraIsOn.observe(this) { cameraOn ->
            updateCameraControls(cameraOn)
            updateFlashlightControlVisibility()
            if (cameraOn) {
                mOpenCvCameraView.enableView()
            } else {
                mOpenCvCameraView.disableView()
            }
        }

        viewModel.flashIsOn.observe(this) {
            updateFlashControls()
        }

        // Init OPENCV
        if (OpenCVLoader.initLocal()) {
            Log.i(TAG, "OpenCV loaded successfully")
        } else {
            Log.e(TAG, "OpenCV initialization failed!")
            (Toast.makeText(this, "OpenCV initialization failed!", Toast.LENGTH_LONG)).show()
            return
        }

        // Prepare recognizer
        processor.initLargeArrays()
        recognizer.init()
        recognizer.setOnPlateRecognizedCallback {
            viewBinding.recognizedView.text = if (viewModel.cameraIsOn.value!!) it else ""

            if (it.isNotEmpty()) {
                val cleanInput = cleanupLP((it))
                if (cleanInput.isNotBlank()) {
                    viewModel.currentLookupStringClean.postValue(cleanInput)
                }
            }
        }

        // Setup Camera View
        mOpenCvCameraView = viewBinding.cameraView
        mOpenCvCameraView.setVisibility(SurfaceView.VISIBLE)
        mOpenCvCameraView.setMaxFrameSize(MAX_FRAME_WIDTH, MAX_FRAME_HEIGHT)
        //mOpenCvCameraView.enableFpsMeter()
        mOpenCvCameraView.isFocusableInTouchMode = true
        mOpenCvCameraView.setCvCameraViewListener(this)

        val cameraAvailable =
            applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)
        if (!cameraAvailable) {
            viewBinding.cameraButton.visibility = View.GONE
        }

        val flashAvailable =
            applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)
        if (!flashAvailable) {
            viewBinding.lightButton.visibility = View.GONE
        }
        cameraManager = getSystemService(Context.CAMERA_SERVICE) as CameraManager
        try {
            cameraId = cameraManager.cameraIdList[0]
        } catch (e: CameraAccessException) {
            Log.e(TAG, "Can not get camera:\n${e.stackTraceToString()}")
        }
        //Ask for permissions
        if (!allPermissionsGranted()) {
            viewBinding.cameraButton.isEnabled = false
            requestPermissions()
        } else {
            onCameraPermissionsGranted()
        }

        viewBinding.cameraButton.setOnClickListener {
            viewModel.toggleCamera()
        }

        viewBinding.lightButton.setOnClickListener {
            toggleFlashlight()
        }
        if (flashAvailable) {
            torchCallback = object : TorchCallback() {
                override fun onTorchModeUnavailable(camId: String) {
                    if (cameraId == camId) {
                        viewModel.flashIsOn.postValue(false)
                    }
                }

                override fun onTorchModeChanged(camId: String, enabled: Boolean) {
                    if (camId == cameraId) {
                        viewModel.flashIsOn.postValue(enabled)
                    }
                }
            }
            cameraManager.registerTorchCallback(torchCallback, null)
        }
    }

    private fun onCameraPermissionsGranted() {
        mOpenCvCameraView.setCameraPermissionGranted()
        viewBinding.cameraButton.isEnabled = true
        viewBinding.cameraLayout.visibility = View.VISIBLE
        viewBinding.recognizedView.visibility = viewBinding.cameraLayout.visibility
        viewBinding.recognizedView.text = ""
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.i(TAG, "onDestroy")
        if (this::torchCallback.isInitialized && this::cameraManager.isInitialized) {
            cameraManager.unregisterTorchCallback(torchCallback)
        }

        if (viewModel.cameraIsOn.value!!) {
            mOpenCvCameraView.disableView()
        }
        processor.destroyLargeArrays()
    }

    override fun onCameraViewStarted(width: Int, height: Int) {

    }

    override fun onCameraViewStopped() {
        //TODO("Not yet implemented")
    }

    override fun onCameraFrame(inputFrame: CameraBridgeViewBase.CvCameraViewFrame?): Mat {
        if (inputFrame == null) {
            return Mat()
        }
        val frame = processor.process(inputFrame)
        if (processor.shieldFound) {
            recognizeShieldImage()
        }
        return frame
    }

    private fun recognizeShieldImage() {
        val shieldImage = processor.shieldImage
        processShieldImage(shieldImage)
        val bitmap =
            Bitmap.createBitmap(shieldImage.width(), shieldImage.height(), Bitmap.Config.ARGB_8888)
        Utils.matToBitmap(shieldImage, bitmap)
        recognizer.process(bitmap)
    }

    private lateinit var tmpMat: Mat
    private val kernelSizeE = 2
    private val kernelSizeD = 4
    private fun processShieldImage(image: Mat) {
        if (!this::tmpMat.isInitialized) {
            tmpMat = Mat(image.size(), image.type())
        }
        val elementD = Imgproc.getStructuringElement(
            Imgproc.CV_SHAPE_RECT,
            Size((2 * kernelSizeD + 1).toDouble(), (2 * kernelSizeD + 1).toDouble()),
            Point(kernelSizeD.toDouble(), kernelSizeD.toDouble())
        )
        val elementE = Imgproc.getStructuringElement(
            Imgproc.CV_SHAPE_RECT,
            Size((2 * kernelSizeE + 1).toDouble(), (2 * kernelSizeE + 1).toDouble()),
            Point(kernelSizeE.toDouble(), kernelSizeE.toDouble())
        )

        Imgproc.erode(image, tmpMat, elementE)
        Imgproc.dilate(tmpMat, image, elementD)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.settings_item -> {
                Log.i(TAG, "Go to Settings...")
                val context: Context = applicationContext
                startActivity(Intent(context, SettingsActivity::class.java))
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume")
        if (viewModel.cameraIsOn.value!!) {
            mOpenCvCameraView.enableView()
        }
        val storedText = viewModel.lastLookupString.value ?: ""
        if (viewBinding.manualPlateInput.text.toString() != storedText) {
            viewBinding.manualPlateInput.setText(storedText)
        }

        sharedPreferences.registerOnSharedPreferenceChangeListener(listener)
        updateManualInputVisibility()
        updateFlashlightControlVisibility()
        updateCameraControlVisibility()
        viewModel.hideGroupInResult.postValue(
            sharedPreferences.getBoolean(
                HIDE_CATEGORY_KEY, false
            )
        )

        onInputChanged()
    }

    override fun onPause() {
        super.onPause()
        Log.i(TAG, "onPause")
        if (viewModel.cameraIsOn.value!!) {
            mOpenCvCameraView.disableView()
        }
        viewModel.storeLastLookup(viewBinding.manualPlateInput.text.toString())
        sharedPreferences.unregisterOnSharedPreferenceChangeListener(listener)
    }

    private fun toggleFlashlight() {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                cameraManager.setTorchMode(cameraId, !viewModel.flashIsOn.value!!)
            }
        } catch (e: CameraAccessException) {
            e.printStackTrace()
        }
    }

    private fun updateFlashControls() {
        viewBinding.lightButton.setImageResource(if (viewModel.flashIsOn.value!!) R.drawable.ic_light_off else R.drawable.ic_light_on)
    }

    private fun updateCameraControls(cameraIsOn: Boolean) {
        viewBinding.cameraButton.setImageResource(if (cameraIsOn) R.drawable.ic_camera_off else R.drawable.ic_camera_on)
        viewBinding.cameraLayout.visibility = if (cameraIsOn) View.VISIBLE else View.GONE

        viewBinding.recognizedView.visibility = viewBinding.cameraLayout.visibility
        viewBinding.recognizedView.text = ""

        viewBinding.lightButton.isEnabled = !cameraIsOn
    }

    private fun updateFlashlightControlVisibility() {
        val controlEnabled = sharedPreferences.getBoolean(FLASHLIGHT_VISIBLE_KEY, true)
        Log.i(TAG, "updateFlashlightControlVisibility controlEnabled=$controlEnabled")
        val cameraIsOn = viewModel.cameraIsOn.value ?: false
        viewBinding.lightButton.visibility = if (controlEnabled && !cameraIsOn) View.VISIBLE else View.GONE
    }

    private fun updateCameraControlVisibility() {
        val controlEnabled = sharedPreferences.getBoolean(CAMERA_CONTROL_VISIBLE_KEY, true)
        Log.i(TAG, "updateCameraControlVisibility controlEnabled=$controlEnabled")
        viewBinding.cameraButton.visibility = if (controlEnabled) View.VISIBLE else View.GONE
        updateCameraControls(viewModel.cameraIsOn.value?:false)
    }

    private fun updateManualInputVisibility() {
        viewBinding.manualInput.visibility =
            if (sharedPreferences.getBoolean(MANUAL_VISIBLE_KEY, true)) View.VISIBLE else View.GONE
    }

    private fun requestPermissions() {
        activityResultLauncher.launch(REQUEST_PERMISSIONS)
    }

    private fun allPermissionsGranted() = REQUEST_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(baseContext, it) == PackageManager.PERMISSION_GRANTED
    }

    private inner class ResultHolder(
        itemView: View,
        val plateTextView: TextView = itemView.findViewById(R.id.foundPlateNumber),
        val groupTextView: TextView = itemView.findViewById(R.id.foundPlateGroup),
        var plate: PlateRecord? = null
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
            plateTextView.setOnClickListener(this)
            groupTextView.setOnClickListener(this)
            itemView.findViewById<ImageView>(R.id.detailIcon).setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            plate?.let { plate ->
                val context = itemView.context
                val builder = AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.car_info_title)).setMessage(
                        context.getString(
                            R.string.info_dialog_template, plate.number, plate.color, plate.extra
                        ).trimMargin()
                    ).setPositiveButton(context.getString(R.string.ok_btn_text), null)
                builder.create().show()
            }
        }

        fun bindResult(plateRec: PlateRecord, groupName: String) {
            plate = plateRec
            plateTextView.text = plateRec.number
            groupTextView.text = groupName
            val groupInvisible = groupName.isEmpty()
            groupTextView.visibility = if (groupInvisible) View.INVISIBLE else View.VISIBLE
        }
    }

    private inner class ResultAdapter(
        val plates: List<Pair<PlateRecord, String>>
    ) : RecyclerView.Adapter<ResultHolder>() {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResultHolder {
            val layoutInflater = LayoutInflater.from(this@MainActivity)
            val view = layoutInflater.inflate(R.layout.delegate_result, parent, false)
            return ResultHolder(view)
        }

        override fun getItemCount(): Int {
            return plates.count()
        }

        @RequiresApi(Build.VERSION_CODES.N)
        override fun onBindViewHolder(holder: ResultHolder, position: Int) {
            val plate = plates[position]
            holder.bindResult(plate.first, plate.second)
        }
    }
}