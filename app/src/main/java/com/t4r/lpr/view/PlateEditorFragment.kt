/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.view

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.t4r.lpr.R
import com.t4r.lpr.cleanupLP
import com.t4r.lpr.db.PlateRecord
import com.t4r.lpr.viewmodel.EditorViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PlateEditorFragment : Fragment() {
    companion object {
        const val REQUEST_ID = "edit"
        const val HAS_UNSAVED_NEW = "has_unsaved_new"
    }

    private var plateNumberText: TextView? = null
    private var plateColorText: TextView? = null
    private var plateDetailsText: TextView? = null

    private var editorMenuBar: Toolbar? = null

    private lateinit var viewModel: EditorViewModel

    private var plate: PlateRecord? = null

    private val uiScope = CoroutineScope(Dispatchers.Main)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity())[EditorViewModel::class.java]

        val view = inflater.inflate(R.layout.plate_editor_fragment, container, false)
        plateNumberText = view.findViewById(R.id.license_plate_text)
        plateColorText = view.findViewById(R.id.plate_color)
        plateDetailsText = view.findViewById(R.id.plate_details_text)

        editorMenuBar = view.findViewById(R.id.plate_editor_action_bar)

        updateUI()
        viewModel.updatingCurrentPlate.observe(viewLifecycleOwner) {
            updateUI()
        }
        return view
    }

    private fun reportHasUnsaved()
    {
        setFragmentResult(REQUEST_ID, bundleOf(HAS_UNSAVED_NEW to true))
    }

    override fun onStop() {
        fun handleUnsavedNewPlate() {
            plate?.let { newPlate ->
                val newNumber = cleanupLP(plateNumberText?.text.toString())
                if (newNumber.isEmpty()) {
                    viewModel.deletePlate(newPlate.id)
                } else {
                    viewModel.storeUnsavedCurrentPlate()
                    reportHasUnsaved()
                }
            }
        }

        if (viewModel.isNewPlate) {
            handleUnsavedNewPlate()
        }
        super.onStop()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editorMenuBar?.inflateMenu(R.menu.plate_edit_menu)
        editorMenuBar?.setNavigationIcon(R.drawable.ic_menu_back)
        editorMenuBar?.setNavigationOnClickListener { findNavController().popBackStack() }
        editorMenuBar?.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_save -> {
                    plate?.let { oldPlate ->
                        val newNumber = cleanupLP(plateNumberText?.text.toString())
                        viewModel.isNewPlate = false
                        viewModel.updatePlate(
                            PlateRecord(
                                oldPlate.id,
                                newNumber,
                                plateColorText?.text.toString(),
                                plateDetailsText?.text.toString(),
                                oldPlate.groupId
                            )
                        ).invokeOnCompletion {
                            uiScope.launch {
                                findNavController().popBackStack()
                            }
                        }
                    }
                    true
                }

                else -> false
            }
        }
        if (viewModel.isNewPlate) {
            editorMenuBar?.setTitle(getString(R.string.new_plate_header))
            plateNumberText?.doOnTextChanged { text, _, _, _ ->
                plate?.let {
                    val newNumber = cleanupLP(text.toString())
                    viewModel.currentPlate = it.copy(number = newNumber)
                }
            }
        } else {
            editorMenuBar?.setTitle(getString(R.string.edit_entry_header))
        }
    }

    private fun updateUI() {
        plate = viewModel.currentPlate

        Log.i(TAG, "isNew=${viewModel.isNewPlate} plate=$plate")
        plateNumberText?.text = plate?.number ?: ""
        plateColorText?.text = plate?.color ?: ""
        plateDetailsText?.text = plate?.extra ?: ""
    }
}