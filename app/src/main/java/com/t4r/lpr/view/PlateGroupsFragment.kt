/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.t4r.lpr.R
import com.t4r.lpr.db.GroupRecordFull
import com.t4r.lpr.viewmodel.EditorViewModel

class PlateGroupsFragment : Fragment() {
    private inner class PlateGroupHolder(
        itemView: View,
        val titleTextView: TextView = itemView.findViewById(R.id.list_item_plate_group_name),
        val countTextView: TextView = itemView.findViewById(R.id.list_item_plate_group_count),
        val editButton: ImageButton = itemView.findViewById(R.id.list_item_edit_button),
        val deleteButton: ImageButton = itemView.findViewById(R.id.list_item_delete_button),
        val usedCheckBox: CheckBox = itemView.findViewById(R.id.list_item_used_check_box),
        var group: GroupRecordFull? = null
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            Log.i(TAG, "onClick")
            group?.let {group ->
                viewModel.selectGroup(group.id)
                v?.findNavController()?.navigate(R.id.platesFragment)
            }
        }

        fun bindGroup(plateGroup: GroupRecordFull) {
            group = plateGroup
            titleTextView.text = plateGroup.name
            countTextView.text =
                if (plateGroup.count == 0) itemView.context.resources.getString(R.string.no_plates_string) else itemView.context.resources.getQuantityString(
                    R.plurals.plate_count_plural, plateGroup.count, plateGroup.count
                )

            usedCheckBox.isChecked = plateGroup.used
            usedCheckBox.setOnCheckedChangeListener { _, isChecked ->
                Log.i(TAG, "usedCheckBox.setOnCheckedChangeListener group=$group")
                group?.let { group->
                    viewModel.updateGroup(group.asGroupRecord().copy(used = isChecked))
                }
            }
            editButton.setOnClickListener { _ ->
                group?.let {group->
                    Log.i(TAG, "Edit clicked for $group")
                    val context = itemView.context

                    val (layout, edit) = createLineEditLayout(context, group.name)

                    val builder = AlertDialog.Builder(context)
                    builder.setTitle(R.string.group_name)
                        .setPositiveButton(R.string.save_btn_text) { _, _ ->
                            val newGroup = group.copy(name = edit.text.toString())
                            viewModel.updateGroup(newGroup.asGroupRecord())
                        }.setNegativeButton(R.string.discard_btn_text) { _, _ -> }.setView(layout)

                    val dialog: AlertDialog = builder.create()
                    dialog.show()
                }
            }
            deleteButton.setOnClickListener { _ ->
                Log.i(TAG, "Delete clicked for $group")
                group?.let {group->
                    val context = itemView.context
                    val builder = AlertDialog.Builder(context)
                    builder.setTitle(R.string.delete_group).setMessage(
                        context.getString(
                            R.string.do_you_want_to_delete_the_group, plateGroup.name
                        )
                    ).setPositiveButton(R.string.delete) { _, _ ->
                        viewModel.deleteGroup(group.id)
                    }.setNegativeButton(R.string.cancel_btn_text) { _, _ -> }
                    builder.create().show()
                }
            }
        }
    }

    companion object {
        fun createLineEditLayout(
            context: Context, groupName: String
        ): Pair<LinearLayoutCompat, EditText> {
            val padding = TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 22F, context.resources.displayMetrics
            ).toInt()
            val layout = LinearLayoutCompat(context)
            layout.setPadding(padding, padding, padding, padding)
            val edit = EditText(context)
            edit.setText(groupName)
            edit.isSingleLine = true
            layout.addView(
                edit, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT
            )
            return Pair(layout, edit)
        }
    }

    private inner class PlateGroupAdapter : RecyclerView.Adapter<PlateGroupHolder>() {
        private val differ = AsyncListDiffer(this, GroupDiffCallback)

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlateGroupHolder {
            val layoutInflater = LayoutInflater.from(activity)
            val view = layoutInflater.inflate(R.layout.delegate_plate_group, parent, false)
            return PlateGroupHolder(view)
        }

        override fun getItemCount() = differ.currentList.count()

        override fun onBindViewHolder(holder: PlateGroupHolder, position: Int) {
            val plateGroup = differ.currentList[position]
            holder.bindGroup(plateGroup)
        }

        fun updateGroups(newGroups: List<GroupRecordFull>) {
            differ.submitList(newGroups)
        }
    }

    private lateinit var viewModel: EditorViewModel
    private lateinit var plateGroupRecyclerView: RecyclerView
    private lateinit var plateGroupAdapter: PlateGroupAdapter
    private var platesMenuBar: Toolbar? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity())[EditorViewModel::class.java]

        val view = inflater.inflate(R.layout.plate_groups_fragment, container, false)
        plateGroupRecyclerView = view.findViewById(R.id.plate_groups_recycler_view)
        plateGroupRecyclerView.layoutManager = LinearLayoutManager(activity)
        plateGroupRecyclerView.addItemDecoration(
            DividerItemDecoration(
                context, LinearLayoutManager.VERTICAL
            )
        )

        platesMenuBar = view.findViewById(R.id.plate_groups_action_bar)

        setupListView()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        platesMenuBar?.inflateMenu(R.menu.groups_menu)
        platesMenuBar?.setNavigationIcon(R.drawable.ic_menu_back)
        platesMenuBar?.setNavigationOnClickListener { activity?.finish() }
        platesMenuBar?.setTitle(R.string.groups_header)
        platesMenuBar?.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_add_group -> {
                    viewModel.addGroup(getString(R.string.created_group_name), true)
                    true
                }

                else -> false
            }
        }
    }

    object GroupDiffCallback : DiffUtil.ItemCallback<GroupRecordFull>() {
        override fun areItemsTheSame(oldItem: GroupRecordFull, newItem: GroupRecordFull): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: GroupRecordFull, newItem: GroupRecordFull
        ): Boolean {
            return oldItem == newItem
        }
    }

    private fun setupListView() {
        Log.i(TAG, "updateUI")
        plateGroupAdapter = PlateGroupAdapter()
        viewModel.groups.observe(viewLifecycleOwner) {
            plateGroupAdapter.updateGroups(it)
        }
        plateGroupRecyclerView.adapter = plateGroupAdapter
    }
}