/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.view

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.doyaaaaaken.kotlincsv.dsl.csvWriter
import com.t4r.lpr.R
import com.t4r.lpr.db.PlateRecord
import com.t4r.lpr.viewmodel.EditorViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.BufferedOutputStream
import java.io.OutputStream
import java.lang.ref.WeakReference

object PlateDiffCallback : DiffUtil.ItemCallback<PlateRecord>() {
    override fun areItemsTheSame(oldItem: PlateRecord, newItem: PlateRecord): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: PlateRecord, newItem: PlateRecord): Boolean {
        return oldItem.id == newItem.id && oldItem.number == newItem.number
    }
}

class PlatesFragment : Fragment() {
    private inner class PlateHolder(
        itemView: View,
        val titleTextView: TextView = itemView.findViewById(R.id.list_item_plate_text),
        val deleteButton: ImageButton = itemView.findViewById(R.id.list_item_delete_button),
        var plateId: Long? = null
    ) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        init {
            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            viewModel.selectPlate(plateId)
            plateId?.let {
                viewModel.isNewPlate = false
                v?.findNavController()?.navigate(R.id.plateEditorFragment)
            }
        }

        fun bindPlate(plate: PlateRecord) {
            titleTextView.text = plate.number
            plateId = plate.id
            deleteButton.setOnClickListener { _ ->
                Log.i(TAG, "Delete clicked for ${plate.id}")
                val context = itemView.context
                val builder = AlertDialog.Builder(context)
                builder.setTitle(R.string.delete_plate).setMessage(
                    context.getString(
                        R.string.do_you_want_to_delete_the_plate, plate.number
                    )
                ).setPositiveButton(R.string.delete) { _, _ ->
                    viewModel.deletePlate(plate.id)
                }.setNegativeButton(R.string.cancel_btn_text) { _, _ -> }
                builder.create().show()
            }
        }
    }

    private inner class PlateAdapter : RecyclerView.Adapter<PlateHolder>() {
        private val differ = AsyncListDiffer(this, PlateDiffCallback)
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlateHolder {
            val layoutInflater = LayoutInflater.from(activity)
            val view = layoutInflater.inflate(R.layout.delegate_plate, parent, false)
            return PlateHolder(view)
        }

        override fun getItemCount() = differ.currentList.count()

        override fun onBindViewHolder(holder: PlateHolder, position: Int) {
            val plate = differ.currentList[position]
            holder.bindPlate(plate)
        }

        fun updatePlates(newPlates: List<PlateRecord>) {
            differ.submitList(newPlates)
        }
    }

    private lateinit var platesRecyclerView: RecyclerView
    private lateinit var plateAdapter: PlateAdapter

    private var platesMenuBar: Toolbar? = null

    private lateinit var viewModel: EditorViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(PlateEditorFragment.REQUEST_ID) { _, bundle ->
            if (!bundle.getBoolean(
                    PlateEditorFragment.HAS_UNSAVED_NEW, false
                )
            ) return@setFragmentResultListener

            Log.i(TAG, "Unsaved plate currentPlate ${viewModel.currentPlate}")
            viewModel.currentPlate?.let { plate ->
                Log.i(TAG, "plate: $plate")
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle("Save changes?").setMessage(
                    requireContext().getString(
                        R.string.do_you_want_to_save_the_plate, plate.number
                    )
                ).setNegativeButton(getString(R.string.discard_btn_text)) { _, _ ->
                    deletePlate(plate.id)
                }.setPositiveButton(getString(R.string.save_btn_text)) { _, _ ->
                    viewModel.updatePlate(
                        PlateRecord(
                            plate.id, plate.number, plate.color, plate.extra, plate.groupId
                        )
                    )
                }
                builder.create().show()
            }
        }
    }

    private fun deletePlate(id: Long) {
        viewModel.deletePlate(id)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(requireActivity())[EditorViewModel::class.java]

        val view = inflater.inflate(R.layout.plate_group_fragment, container, false)
        platesRecyclerView = view.findViewById(R.id.plate_group_recycler_view)
        platesRecyclerView.layoutManager = LinearLayoutManager(activity)
        platesRecyclerView.addItemDecoration(
            DividerItemDecoration(
                context, LinearLayoutManager.VERTICAL
            )
        )

        platesMenuBar = view.findViewById(R.id.plate_group_action_bar)

        setupListView()
        return view
    }

    private val uiScope = CoroutineScope(Dispatchers.Main)

    private val requestCreateDocument =
        registerForActivityResult(ActivityResultContracts.CreateDocument("text/csv")) {
            it?.let { uri ->
                Log.i(TAG, "URI: $uri")
                val outputStream = context?.contentResolver?.openOutputStream(uri)
                activity?.let { context ->
                    outputStream?.let { stream ->
                        viewModel.plates.value?.let { plates ->
                            uiScope.launch {
                                writeAsCSV(plates, stream, WeakReference(context))
                            }
                        }
                    }
                }
            }
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        platesMenuBar?.inflateMenu(R.menu.plates_menu)
        platesMenuBar?.setNavigationIcon(R.drawable.ic_menu_back)
        platesMenuBar?.setNavigationOnClickListener {
            findNavController().popBackStack()
        }

        viewModel.currentGroup?.let { group -> platesMenuBar?.title = group.name }

        platesMenuBar?.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_add_plate -> {
                    viewModel.newPlate()
                    this.findNavController().navigate(R.id.plateEditorFragment)
                    true
                }

                R.id.action_export_group -> {
                    var suggestedFileName = ""
                    viewModel.currentGroup?.let { g ->
                        suggestedFileName = g.name.replace(" ", "_") + ".csv"
                    }
                    requestCreateDocument.launch(suggestedFileName)
                    true
                }

                else -> false
            }
        }
    }

    private suspend fun writeAsCSV(
        groupPlates: List<PlateRecord>, stream: OutputStream, ctx: WeakReference<Context>
    ) {
        var success = false
        withContext(Dispatchers.IO) {
            try {
                val writer = csvWriter {
                    charset = "UTF-8"
                    delimiter = ','
                    lineTerminator = "\r\n"
                    outputLastLineTerminator = true
                    quote.char = '"'
                }
                val bufferedStream = BufferedOutputStream(stream)
                writer.open(BufferedOutputStream(bufferedStream)) {
                    for (plate in groupPlates) {
                        writeRow(plate.number, plate.color, plate.extra)
                    }
                }
                success = true
            } catch (e: Exception) {
                Log.e(TAG, "Can not save CSV: $e")
            }
            withContext(Dispatchers.Main) {
                ctx.get()?.let { context ->
                    val toast = Toast(context)
                    if (success) {
                        toast.duration = Toast.LENGTH_SHORT
                        toast.setText(R.string.group_export_succeeded)
                    } else {
                        toast.duration = Toast.LENGTH_LONG
                        toast.setText(R.string.group_export_Failed)
                    }
                    toast.show()
                }
            }
        }
    }

    private fun setupListView() {
        plateAdapter = PlateAdapter()
        viewModel.plates.observe(viewLifecycleOwner) {
            plateAdapter.updatePlates(it)
        }
        platesRecyclerView.adapter = plateAdapter
    }
}