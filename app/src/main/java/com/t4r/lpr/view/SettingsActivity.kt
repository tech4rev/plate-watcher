/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.view

import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.room.Room
import com.github.doyaaaaaken.kotlincsv.client.CsvReader
import com.github.doyaaaaaken.kotlincsv.dsl.context.CsvReaderContext
import com.t4r.lpr.R
import com.t4r.lpr.cleanupLP
import com.t4r.lpr.db.GroupRecord
import com.t4r.lpr.db.LongID
import com.t4r.lpr.db.PlateRecord
import com.t4r.lpr.db.PlatesDataBase
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.InputStream

class SettingsActivity : AppCompatActivity() {
    private lateinit var roomDB: PlatesDataBase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings_activity)

        val settingsMenuBar: Toolbar = findViewById(R.id.settings_action_bar)
        settingsMenuBar.setNavigationIcon(R.drawable.ic_menu_back)

        setSupportActionBar(settingsMenuBar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction().replace(R.id.settings, SettingsFragment())
                .commit()
        }

        roomDB = Room.databaseBuilder(
            this.applicationContext, PlatesDataBase::class.java, "plates-db"
        ).build()
    }

    fun showRenameDialog(newGroupId: Long) {
        val builder = AlertDialog.Builder(this)
        val activityContext = this
        CoroutineScope(Dispatchers.Main).launch {
            val newGroup = withContext(Dispatchers.IO) {
                roomDB.groupsDao().getGroup(newGroupId)
            }
            if (newGroup != null) {
                val (layout, edit) = PlateGroupsFragment.createLineEditLayout(
                    activityContext, newGroup.name
                )
                builder.setTitle(getString(R.string.name_for_new_group))
                    .setPositiveButton(R.string.save_btn_text) { _, _ ->
                        CoroutineScope(Dispatchers.IO).launch {
                            roomDB.groupsDao().update(newGroup.copy(name = edit.text.toString()))
                        }
                    }.setNegativeButton(R.string.discard_btn_text) { _, _ ->
                        CoroutineScope(Dispatchers.IO).launch {
                            roomDB.groupsDao().deleteById(LongID(newGroupId))
                        }
                    }.setView(layout)

                val dialog: AlertDialog = builder.create()
                dialog.show()
            }
        }
    }

    class SettingsFragment : PreferenceFragmentCompat() {
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey)
        }

        private val requestPickCSV =
            registerForActivityResult(ActivityResultContracts.OpenDocument()) {
                it?.let { uri ->
                    var newGroupId: Long = -1L
                    val activityContext = requireActivity() as SettingsActivity
                    val roomDB = activityContext.roomDB

                    Log.i(TAG, "URI: $uri")
                    val type = activity?.contentResolver?.getType(uri)
                    Log.i(TAG, "URI type: $type")
                    var groupName: String = getString(R.string.imported_group_name)
                    context?.contentResolver?.query(
                        uri, arrayOf(MediaStore.MediaColumns.DISPLAY_NAME), null, null, null
                    )?.use { cursor ->
                        if (cursor.moveToFirst()) {
                            groupName = cursor.getString(0)
                            if (groupName.endsWith(".csv", true)) {
                                groupName = groupName.dropLast(4)
                            }
                        }
                    }
                    Log.i(TAG, "groupName: '$groupName'")
                    CoroutineScope(Dispatchers.IO).launch {
                        activity?.contentResolver?.openInputStream(uri)?.use { input ->
                            newGroupId = createGroup(roomDB, groupName, input)
                        }
                    }.invokeOnCompletion {
                        CoroutineScope(Dispatchers.Main).launch {
                            handleImportCompleted(newGroupId)
                        }
                    }
                }
            }

        private fun handleImportCompleted(newGroupId: Long) {
            if (newGroupId == -1L) {
                Toast.makeText(
                    activity, getString(R.string.import_failed_toast), Toast.LENGTH_LONG
                ).show()
            } else {
                Toast.makeText(
                    activity, getString(R.string.group_imported_toast), Toast.LENGTH_SHORT
                ).show()
                when (val settingsActivity = activity) {
                    is SettingsActivity -> settingsActivity.showRenameDialog(
                        newGroupId
                    )
                }
            }
        }

        override fun onPreferenceTreeClick(preference: Preference) =
            if (preference.key == "IMPORT_CSV") {
                requestPickCSV.launch(arrayOf("text/*"))
                true
            } else {
                super.onPreferenceTreeClick(preference)
            }

        private fun createGroup(
            roomDB: PlatesDataBase, groupName: String, input: InputStream
        ): Long {
            var newGroupId = roomDB.groupsDao().insertRaw(GroupRecord(0L, groupName, true))
            var numAddedPlates = 0
            roomDB.runInTransaction {
                try {
                    val csvContext = CsvReaderContext().apply {
                        charset = Charsets.UTF_8.name()
                        quoteChar = '"'
                        delimiter = ','
                        skipEmptyLine = true
                    }
                    val reader = CsvReader(csvContext)
                    reader.open(input) {

                        val sequence = readAllAsSequence()
                        sequence.forEach { row ->
                            if (row.size == 3) {
                                val origNumber = row[0]
                                val color = row[1]
                                val cleanPlateNumber = cleanupLP(origNumber)
                                val description = if (origNumber == cleanPlateNumber) {
                                    row[2]
                                } else {
                                    origNumber + '\n' + row[2]
                                }
                                roomDB.platesDao().insertRaw(
                                    PlateRecord(
                                        0L, cleanPlateNumber, color, description, newGroupId
                                    )
                                )
                                numAddedPlates += 1
                            }
                        }
                        Log.i(TAG, "Added $numAddedPlates plates.")
                    }
                } catch (e: Exception) {
                    Log.e(TAG, "Error: $e")
                }
            }
            if (numAddedPlates <= 0) {
                Log.e(TAG, "No plates added for Group $newGroupId !")
                roomDB.groupsDao().deleteByIdRaw(LongID(newGroupId))
                newGroupId = -1L
            }
            return newGroupId
        }
    }
}