/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.t4r.lpr.db.GroupRecord
import com.t4r.lpr.db.GroupRecordFull
import com.t4r.lpr.db.LongID
import com.t4r.lpr.db.PlateRecord
import com.t4r.lpr.db.PlatesDataBase
import com.t4r.lpr.view.TAG
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class EditorViewModel : ViewModel() {
    var currentGroup: GroupRecord? = null
    var currentPlate: PlateRecord? = null
    var isNewPlate = false
    val updatingCurrentPlate = MutableLiveData(false)
    val groups = MutableLiveData<List<GroupRecordFull>>()
    val plates = MutableLiveData<List<PlateRecord>>()

    private lateinit var db: PlatesDataBase

    fun setDBModel(platesDB: PlatesDataBase) {
        db = platesDB
        reloadGroups()
    }

    private fun reloadGroups() {
        viewModelScope.launch(Dispatchers.IO) {
            val groupsFull = db.groupsDao().getGroupsFull()
            Log.i(TAG, "groupsFull = $groupsFull")
            groups.postValue(groupsFull)
        }
    }

    fun selectGroup(groupId: Long) {
        plates.postValue(emptyList())
        currentGroup = null
        val found = groups.value?.find { it.id == groupId }
        if (found != null) {
            currentGroup = found.asGroupRecord()
            viewModelScope.launch(Dispatchers.IO) {
                val groupPlates = db.platesDao().getGroupPlates(groupId)
                plates.postValue(groupPlates)
            }
        }
    }

    fun selectPlate(plateId: Long?) {
        currentPlate = null
        if (plateId == -1L) {
            return
        }
        updatingCurrentPlate.postValue(true)
        plateId?.let { id ->
            viewModelScope.launch(Dispatchers.IO) {
                currentPlate = db.platesDao().getPlate(id)
                updatingCurrentPlate.postValue(false)
                Log.i(TAG, "inserted plate $currentPlate")
            }
        }
    }

    fun newPlate() {
        isNewPlate = true
        updatingCurrentPlate.postValue(true)
        currentPlate = null
        viewModelScope.launch(Dispatchers.IO) {
            val plate = PlateRecord(0, "", "", "", currentGroup?.id ?: 0)
            val plateId = db.platesDao().insert(plate)
            currentPlate = plate.copy(id = plateId)
            reloadPlates()
            reloadGroups()
            updatingCurrentPlate.postValue(false)
        }
    }

    fun updatePlate(plate: PlateRecord) : Job {
        currentPlate?.let { current ->
            if (current.id == plate.id) {
                currentPlate = plate
            }
        }
        return viewModelScope.launch(Dispatchers.IO) {
            db.platesDao().update(plate)
            Log.i(TAG, "updated!")
            reloadPlates()
            Log.i(TAG, "reloaded!")
        }
    }

    private fun reloadPlates() {
        currentGroup?.let {
            viewModelScope.launch(Dispatchers.IO) {
                val groupPlates = db.platesDao().getGroupPlates(it.id)
                plates.postValue(groupPlates)
            }
        }
    }

    fun updateGroup(asGroupRecord: GroupRecord) {
        currentGroup?.let { current ->
            if (current.id == asGroupRecord.id) {
                currentGroup = asGroupRecord
            }
        }
        viewModelScope.launch(Dispatchers.IO) {
            Log.i(TAG, "update: $asGroupRecord")
            db.groupsDao().update(asGroupRecord)
            reloadGroups()
        }
    }

    fun deleteGroup(id: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            db.groupsDao().deleteById(LongID(id))
            reloadGroups()
        }
    }

    fun addGroup(name: String, used: Boolean) {
        viewModelScope.launch(Dispatchers.IO) {
            db.groupsDao().insert(GroupRecord(id = 0, name, used))
            reloadGroups()
        }
    }

    fun deletePlate(id: Long) {
        viewModelScope.launch(Dispatchers.IO) {
            db.platesDao().deleteById(LongID(id))
            reloadPlates()
            reloadGroups()
        }
    }

    fun storeUnsavedCurrentPlate() {
        currentPlate?.let { plate ->
            updatePlate(plate)
        }
    }
}