/*
 * This file is part of the PlateWatcher app.
 * Copyright (c) 2024 John Doe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PlateWatcher. If not, see <https://www.gnu.org/licenses/>.
 */

package com.t4r.lpr.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.t4r.lpr.db.PlateRecord
import com.t4r.lpr.db.PlateWithGroupName
import com.t4r.lpr.db.PlatesDataBase
import com.t4r.lpr.getCleanCombinations
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

const val TAG = "LookupViewModel"

class LookupViewModel(private val savedStateHandle: SavedStateHandle) : ViewModel() {
    val flashIsOn = MutableLiveData(false)
    val cameraIsOn = MutableLiveData(false)

    val hideGroupInResult = MutableLiveData(false)
    val lastLookupString = savedStateHandle.getLiveData<String>("last_lookup")

    val currentLookupStringClean = MutableLiveData("")

    private val exactLookupResult = MediatorLiveData<List<Pair<PlateRecord, String>>>(emptyList())
    val shownExactLookupResult = MediatorLiveData<List<Pair<PlateRecord, String>>>(emptyList())

    private val fuzzyLookupResult = MutableLiveData<List<Pair<PlateRecord, String>>>(emptyList())
    val showFuzzyLookupResult = MediatorLiveData<List<Pair<PlateRecord, String>>>(emptyList())

    init {
        exactLookupResult.apply {
            addSource(currentLookupStringClean) {
                Log.i(TAG, "currentLookupStringClean changed")
                doLookup()
            }
        }
        shownExactLookupResult.apply {
            addSource(exactLookupResult) {
                updateShownResult(exactLookupResult, shownExactLookupResult)
            }
            addSource(hideGroupInResult) {
                updateShownResult(exactLookupResult, shownExactLookupResult)
            }
        }

        showFuzzyLookupResult.apply {
            addSource(fuzzyLookupResult) {
                updateShownResult(fuzzyLookupResult, showFuzzyLookupResult)
            }
            addSource(hideGroupInResult) {
                updateShownResult(fuzzyLookupResult, showFuzzyLookupResult)
            }
        }
    }

    private fun updateShownResult(
        lookupResult: LiveData<List<Pair<PlateRecord, String>>>,
        shownResult: MutableLiveData<List<Pair<PlateRecord, String>>>
    ) {
        Log.i(TAG, "updateShownResult")
        val hideGroupNames = hideGroupInResult.value ?: false
        shownResult.value = lookupResult.value?.map { pair ->
            Pair(
                pair.first, if (hideGroupNames) "" else pair.second
            )
        }
    }

    private lateinit var db: PlatesDataBase
    private fun doLookup() {
        Log.i(TAG, "doLookup()")
        val plateNumber = currentLookupStringClean.value
        if (plateNumber.isNullOrEmpty()) {
            exactLookupResult.postValue(emptyList())
            fuzzyLookupResult.postValue(emptyList())
            return
        }

        doExactLookup(plateNumber)
    }

    private fun doExactLookup(plateNumber: String) {
        Log.i(TAG, "doExactLookup($plateNumber)")
        viewModelScope.launch(Dispatchers.IO) {
            val exact = db.platesDao().lookupPlatesInUsedGroupsByNumberRaw(plateNumber)
                .map { makePairFromResult(it) }
            exactLookupResult.postValue(exact)
            if (exact.isEmpty()) {
                fuzzyLookup(plateNumber)
            } else {
                fuzzyLookupResult.postValue(emptyList())
            }
        }
    }

    private fun makePairFromResult(item: PlateWithGroupName) = Pair(
        PlateRecord(item.id, item.number, item.color, item.extra, item.groupId), item.groupName
    )

    private fun fuzzyLookup(plateNumber: String) {
        Log.i(TAG, "fuzzyLookup($plateNumber)")

        val combinations = getCleanCombinations(plateNumber).filterNot { it == plateNumber }
        Log.i(TAG, "fuzzyCombinations: $combinations")

        if (combinations.isEmpty()) {
            fuzzyLookupResult.postValue(emptyList())
            return
        }

        viewModelScope.launch(Dispatchers.IO) {
            val matchedRecords = ArrayList<Pair<PlateRecord, String>>()
            combinations.forEach { variation ->
                matchedRecords.addAll(db.platesDao().lookupPlatesInUsedGroupsByNumberRaw(variation)
                    .map {
                        makePairFromResult(it)
                    })
            }
            fuzzyLookupResult.postValue(matchedRecords)
        }
    }

    fun storeLastLookup(lookupString: String) {
        Log.i(TAG, "storeLastLooup: $lookupString")
        savedStateHandle["last_lookup"] = lookupString
    }

    fun setDBModel(roomDB: PlatesDataBase) {
        db = roomDB
    }

    fun toggleCamera() {
        cameraIsOn.postValue(!cameraIsOn.value!!)
    }
}