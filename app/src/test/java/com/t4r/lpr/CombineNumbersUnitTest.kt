package com.t4r.lpr

import org.junit.Assert.assertEquals
import org.junit.Test

class CombineNumbersUnitTest {
    @Test
    fun cleanupLP_keepsGoodPlate_unchanged() {
        assertEquals("BB1234", cleanupLP("BB1234"))

        assertEquals("XX234", cleanupLP("XX234"))

        assertEquals("", cleanupLP(""))
    }

    @Test
    fun cleanupLP_trims() {
        assertEquals("BB1234", cleanupLP(" BB1234 "))
        assertEquals("", cleanupLP(" "))
    }

    @Test
    fun cleanupLP_removes_space() {
        assertEquals("BB1234", cleanupLP("BB 123 4"))
    }

    @Test
    fun cleanupLP_removes_dash() {
        assertEquals("BB-1234", cleanupLP("BB-1234"))
    }

    @Test
    fun combine_nonAmbiguousGivesSingleResult() {
        val s = "TT4LK44"
        assertEquals(/* expected = */ arrayListOf(s), /* actual = */ combineNumbers(s))

        val s2 = "YN4MJ43"
        assertEquals(/* expected = */ arrayListOf(s2), /* actual = */ combineNumbers(s2))
    }

    @Test
    fun combine_ambiguousGivesMultipleResults() {
        val s = "T04MK44"
        assertEquals(/* expected = */ arrayListOf(s, "TO4MK44", "TÖ4MK44"), /* actual = */ combineNumbers(s))

        val s2 = "T04IK44"
        assertEquals(/* expected = */ arrayListOf(
            "T04IK44", "T041K44", "TO4IK44", "TO41K44", "TÖ4IK44", "TÖ41K44"
        ).sorted(), /* actual = */ combineNumbers(s2).sorted()
        )
    }

    @Test
    fun combine_ambiguousShortGivesMultipleResults() {
        val s = "18"
        assertEquals(/* expected = */ setOf("18", "1B", "1H", "I8", "IB", "IH"), /* actual = */
            combineNumbers(s).asIterable().toSet()
        )

        val s2 = "1"
        assertEquals(/* expected = */ setOf("1", "I"), /* actual = */
            combineNumbers(s2).asIterable().toSet()
        )
        val s3 = "8"
        assertEquals(/* expected = */ setOf("8", "B", "H"), /* actual = */
            combineNumbers(s3).asIterable().toSet()
        )
    }

    @Test
    fun combine_umlautsHandled() {
        val s1 = "AT"
        assertEquals(setOf("AT", "ÄT"), combineNumbers(s1).asIterable().toSet())

        val s2 = "ÜT"
        assertEquals(setOf("UT", "ÜT"), combineNumbers(s2).asIterable().toSet())

        val s3 = "ÖT"
        assertEquals(setOf("OT", "ÖT", "0T"), combineNumbers(s3).asIterable().toSet())
    }

}